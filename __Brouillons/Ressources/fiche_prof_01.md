# Fiche professeur

**Thématique :** notions transversales de programmation.

**Notions liées :** affectations, variables, séquences, instructions conditionnelles, boucles bornées et non bornées, définitions et appels de fonctions, écrire et développer des programmes pour répondre à des problèmes et modéliser des phénomènes physiques, économiques et sociaux.

**Résumé de l’activité :** voir les notions de bases en programmation
en utilisant le module turtle de python pour dessiner.

**Objectifs :** présenter la syntaxe python, la programmation séquentielle et textuelle.

**Auteur :** Maxime Fourny maxime-simon.fourny@ac-besancon.fr

**Durée de l’activité :** 3H peut être faite en coordination avec le
cours de mathématiques.

**Forme de participation :** individuelle en autonomie

**Matériel nécessaire :** ordinateur avec installé un interprète Python et le module `Turtle`

**Préparation :** aucune

---

## Déroulement

Après une présentation succincte de python (moins de deux minutes)
je les laisse aller à leur rythme.

**Etape 1 :** Les exercices 2 et 3 permettent de rentrer dans le logiciel
et de prendre en main python. Ils sont tapés directement, instruction par instruction, dans l’interpréteur. A ce stade, je ne leur montre pas encore le bloc-note.

**Etape 2 :** L’exercice 5 me permet d’agacer la plupart des élèves qui
se voient obligés de tout effacer car ils ont tendance à trouver la longueur de la diagonale par tatonnement avant d’arriver à mobiliser _Pythagore_. C’est l’occasion de leur présenter le bloc-note et la programmation textuelle ainsi que l’intérêt d’écrire son programme dans un fichier texte avant de l'exécuter.

On peut observer chez certains élèves des stratégies de contournement consistant à changer la couleur en _white_ pour effacer. Cela leur évite d’avoir à tout effacer.

C’est également l’occasion de revenir sur l’exercice 4 et de leur
montrer qu’il est identique au 2 en rajoutant une seule instruction au début.

**Etape 3 :** Les exercices 6 et 7 me servent à faire patienter les élèves
performants.

Je passe directement sur l’exercice 8 après l’exercice 5 lorsque
la totalité des élèves a réussi le 5.

**Etape 4 :** Les exercices 8 à 11 imposent aux élèves une recherche de
l’angle. Certains trouvent directement la bonne formule mais la majorité procède par tatonnement et s’agace de devoir modifier toutes leurs lignes de code. J’en profite pour leur présenter l’affectation : `angle = 37`.

**Etape 5 :** L’exercice 11 voit les élèves faire un polygône à 360 côtés
en général. Certains me demandent si il n’y a pas moyen de
faire _répéter_. Les autres s’agaçant de devoir faire un copier-coller. Je leur présente alors la syntaxe de la boucle bornée.

Certains _petits malins_ trouvent la fonction `circle`. Je leur demande alors de voir si il n’en existe pas une pour faire un carré. Après quelques essais infructueux, ils se convainquent que ce n’est pas le cas.

**Etape 6 :** L’exercice 12 me permet de leur montrer la syntaxe d’une
procédure/fonction (sans paramètre) afin de créer une telle fonction par nous même. Je présente la possibilité de mettre des arguments aux élèves qui m’en font la demande.

**Etape 7 :** Les exercices 13 et 14 permettent de montrer l’intérêt
d’une fonction à paramètre.

C’est l’occasion de revenir sur les exercices demandant de tracer des polygônes et de montrer que le carré, le décagone etc... peuvent s’hériter de la fonction suivante :

```python
def polygone (n, cote ) :
    """
    n represente ne nombre de côté
    cote est la longueur du côté
    dessine un polygone
    """
    angle = 360 / n
    for k in range (n):
        forward(cote)
        left(angle)

def carre(cote):
    polygone(4, cote)

def decagone(cote):
    polygone(10, cote)
```

L’exercice 14 permet également d’introduire l’utilisation du `k` dans la boucle de répétition.

**Etape 8 :** L’exercice 15 introduit la double boucle `for`

**Etape 9 :** L’exercice 16 permet d’introduire la syntaxe d’une instruction conditionnelle.

**Etape 10 :** Les exercices suivants me permettent de donner des
exercices un peu _défi_ à certains élèves performants.

## Conclusion

### Qu’est ce que j’ai appris ? 

### Qu’est ce qui était difficile ?

## Pour aller plus loin
