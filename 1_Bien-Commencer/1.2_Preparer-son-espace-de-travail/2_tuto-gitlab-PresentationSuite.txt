## Pour le Mooc NSI : Comment on s’organise ?

Pour ce Mooc “Apprendre à enseigner le Numérique et les Sciences Informatiques” les deux grands espaces de fichiers important sont :

- [MOOC 2 _ Pratique](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique) : c’est l'espace de tous les contenus du MOOC “Apprendre à enseigner le Numérique et les Sciences Informatiques” à votre disposition en lecture

- [MOOC 2 _ Ressources](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources) : c’est le modèle à partir duquel vous allez créer votre espace de travail. Pour faire de cet espace votre espace de travail, on vous explique comment faire [ici]. L'idée sera de "forker" ce modèle et ensuite vous travaillerez à loisir dedans (sans tenter de "merger" avec le site initial pour bien le laisser en l'état)
