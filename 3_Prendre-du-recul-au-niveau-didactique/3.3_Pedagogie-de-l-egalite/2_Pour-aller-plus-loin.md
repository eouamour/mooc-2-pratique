### Pour aller plus loin : les tables rondes sur la pédagogie de l'égalité

Ressources issues du Plan National de Formaton à l'égalité filles-garçons: pour la mixité dans les formations et les métiers du numérique, du 11 mai au 17 juin 2021.

#### Contexte

Plusieurs études montrent un recul de la mixité dans les filières et les métiers du numérique, tout récemment  Gender Scan dans l’innovation en 2019 avec une spécificité française : une chute de la proportion de diplômées dans le numérique (-2 % en France contre + 20 % en UE) ; baisse de la proportion de femmes actives dans les emplois de la haute technologie, progression ralentie de la mixité parmi les spécialistes en TIC (supérieure à 20 % en Europe contre 12 % en France).

Ce sont davantage des inégalités d'orientation que de réussite qui engendrent des inégalités de carrière entre les sexes. En effet, à l’école et au collège, filles et garçons ont des résultats identiques en mathématiques et dans les matières scientifiques et technologiques. Dans le numérique, c’est plutôt même à l’avantage des filles, selon une enquête internationale à laquelle la France a participé pour la première fois en 2018 (ICILS international computer and information Litteracy Study).  En 4ème les filles réussissent bien mieux en littéracie numérique et sont à égal niveau en pensée informatique.  C’est plus tard au cours de la scolarité, au moment de faire des choix de filières, que les différences apparaissent. Au lycée les filles représentaient, jusqu’en 2018, moins de 10 % des élèves des spécialités liées à l’informatique. En 2020 2,9 % d’entre elles (contre 16,8 % les garçons) ont choisi la nouvelle spécialité numérique et informatique offerte en classe de Première.

Plusieurs raisons expliquent cet état de fait : tout particulièrement le déficit de culture numérique en général, la très grande méconnaissance des métiers qui souffrent d’une image « technique » peu attractive et la persistance de stéréotypes et biais de genre qui au fur et à mesure écartent les filles de ces filières. Les métiers du numérique sont au moins au nombre de 80 et quand on interroge les élèves, ils ne connaissent que celui « d’informaticien ». A fortiori la transformation des métiers avec le numérique, ceux de la médecine, du droit, de l’agronomie ou de l’environnement, secteurs particulièrement féminisés, est ignorée. 

Constat aujourd’hui : ce déséquilibre a des conséquences économiques et sociétales. L’enjeu de féminiser ces métiers est réellement un enjeu de démocratie et de citoyenneté. 

Le ministère de l'Éducation Nationale et de la jeunesse et des sports, affiche une politique ambitieuse construite en interministériel, avec une mise en synergie de partenaires-clés instutionnels et associatifs : la DNE, la DGESCO, la Fondation femmes@numérique, l’ONISEP, Canopé, Inria, les entreprises (prioritairement celles fédérées par le CIGREF, l’AFINEF, EdtechFrance), un collectif d’associations. Les actions communes doivent sont abordées de façon transversale à l’ensemble des actions, afin d’avoir un réel impact sur l’augmentation du nombre de filles et de femmes dans les métiers du numérique. Les actions conduites doivent s’appuyer sur les résultats de la recherche et leur impact évalué en continu car de l’avis de tous les professionnels, celles qui ont été conduites jusque-là ont plutôt peu d’effet. 

#### Table ronde 1. Pourquoi la mixité dans le numérique est-elle un enjeu de société ?

- Animation/Intervention : Jean-Marie Chesneaux, IGESR
- _Le constat._ Claudine Schmuck Global Contact étude Gender Scan 
- _Pourquoi est-ce un enjeu de société ? Pourquoi en est-on arrivé là ?_ Muriel Brunet, Cheffe de projet Filles et numérique, DNE et Henri d’Agrain, Délégué général du Cigref 
- _Quel est le rôle de l’Education nationale ?_ Claude Roiron, déléguée ministérielle à l’égalité filles garçons
- _Exemple : Parallaxe 2050 (un container escape game)_ Xavier Cheney, Directeur Opérationnel du Campus des métiers et des qualifications Drômes Ardèche
  - [La vidéo.](https://files.inria.fr/LearningLab_public/C045TV/ITW/EFG_Num_TR1.mp4)

#### Table ronde 2. Comment enseigner un numérique égalitaire ? 

- Animation/Intervention : Sylvie Kocik, Déléguée régionale académique au numérique Région Nouvelle Aquitaine
- _Comment enseigner et transmettre un numérique égalitaire éclairé par la Recherche ?_ Isabelle Collet, Professeure en science de l’éducation à l’Université de Genève
- _Inventer des pratiques pédagogiques innovantes._ Sophie Viger, Directrice de 42, ex-directrice de la WebAcadémy 
- _Former les enseignants aux pratiques pédagogiques innovantes._ Marie-Caroline Missir, Directrice générale de Canopé
- _Comment évaluer l’impact des actions_. Dorothée Roch
- _Exemple : Livret tous.tes numérique_. Soledad Tolosa, Les Petits Débrouillards et Alter Egaux 
- _Exemple : Les Intrépides de la Tech_. Lucie Jagu, Fondation SIMPLON ou Claude Terrosier Magic Makers
  - [La vidéo.](https://files.inria.fr/LearningLab_public/C045TV/ITW/EFG_Num_TR2.mp4)

#### Table ronde 3. Les femmes dans les métiers du numérique

- Animation : Sabrina Caliaros Déléguée académique au numérique de l’académie de Montpellier 
- _L’orientation en question_. Frédérique Alexandre-Bally, Directrice Générale de l’Onisep et Philippe Lebreton, Chef du bureau l'orientation et de la lutte contre le décrochage scolaire
- _La mobilisation des entreprises et des réseaux_. Consuelo Benicourt, Directrice RSE Sopra Stéria et Muriel Brunet, Cheffe de projet Filles et numérique, DNE 
- _Exemple : La boite à métiers du numérique … et du genre_. Anaïs Moressa, Science animation
  - [La vidéo.](https://files.inria.fr/LearningLab_public/C045TV/ITW/EFG_Num_TR3.mp4)
