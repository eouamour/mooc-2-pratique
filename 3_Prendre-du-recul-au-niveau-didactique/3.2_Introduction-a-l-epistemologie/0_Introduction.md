Cette section est consacrée à l’étude de l’interaction conceptuelle entre l’informatique et les autres disciplines scientifiques. Elle a pour but de montrer qu’une discipline acquiert sa maturité scientifique et peut prétendre au statut de science lorsqu’on peut faire une étude comparative de ses connaissances propres et de ses échanges avec les autres domaines. 

Elle comprend : 

1. Une définition de la notion d’épistémologie et une approche de sa restriction à une discipline donnée ici l'informatique, 
2. un commentaire et des définitions sur le statut de science et de technologie, en introduisant les notions de filiation scientifique et de fécondation croisée des modèles 
3. Une instantiation de ses notions en ce qui concerne l’informatique, sa naissance, son évolution 
4. Les échanges conceptuels et les services rendus par l’informatique à son environnement, en particulier scientifique 
5. La notion de pluridisciplinarité, et la position d’appartenance disciplinaire, d’image et de rôle à la fois scientifique et social. 
